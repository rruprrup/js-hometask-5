'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
    let removedBlock = document.getElementById('deleteMe');
    removedBlock.remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
    let lists = document.querySelector(".calc-list");
    let divs = lists.querySelectorAll(".wrapper");
    divs.forEach((div) => {
        let paragraphs = div.querySelectorAll("p");
        let summ = 0;
        paragraphs.forEach((p) => {
            summ += Number(p.textContent);
            div.removeChild(p);
        });
        let newP = document.createElement("p");
        newP.textContent = summ;
        div.appendChild(newP);
    });
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
    let input = document.querySelector("#changeMe");
    input.setAttribute("type", "text");
    input.setAttribute("value", "not_a_secret_anymore");
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
    let list = document.querySelector("#changeChild");
    let liFirst = document.createElement("li");
    liFirst.textContent = "1";
    let liThird = document.createElement("li");
    liThird.textContent = "3";
    list.appendChild(liThird);
    list.insertBefore(liFirst, list.children[1]);
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
    let item = document.querySelectorAll(".item");
    for (let i = 0; i < item.length; i++) {
        if (item[i].classList.contains("red")) {
            item[i].classList.remove("red");
            item[i].classList.add("blue");
        } else if (item[i].classList.contains("blue")) {
            item[i].classList.remove("blue");
            item[i].classList.add("red");
        };
    };
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
