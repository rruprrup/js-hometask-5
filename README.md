# js-hometask-5

## Ответьте на вопросы

1. Какие данные хранятся в следующих свойствах узла: `parentElement`, `children`, `previousElementSibling` и `nextElementSibling`? <br>
`parentElement` — хранит тег (элемент) родитель<br>
`children` —  хранит дочерние теги элемента<br>
`previousElementSibling` —  хранит предыдущий в разметке тег у общего родителя или ничего, если его нет<br>
`nextElementSibling` —  хранит последующий в разметке тег у общего родителя или ничего, если его нет<br>
2. Нарисуй какое получится дерево для следующего HTML-блока.

```html
<p>Hello,<!--MyComment-->World!</p>
```
-> Element: p -> Comment: «<! --MyComment-->» <br>
            p -> Text: «Hello, »<br>
            p -> Text: «World!» <br>
*p — один и тот же


## Выполните задания

1. Клонируй репозиторий;
2. Допиши функции в `tasks.js`;
3. Для проверки работы функций открой index.html в браузере;
4. Создай MR с решением.
